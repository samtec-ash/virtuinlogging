// @Flow

import { VirtuinLogger } from 'VirtuinLogger';

describe('VirtuinLogger opening', () => {
  it('should successfully open', async () => {
    expect.assertions(1);
    process.env.VIRT_STATION_NAME = process.env.VIRT_STATION_NAME || 'VirtionDebug';
    const logger = new VirtuinLogger();
    logger.vtEnableCloudwatch = false;
    const configs = {
      test: {
        testUUID: '123456789',
      },
      station: {
        name: 'debug_station',
      },
      dut: {
      },
    };
    const success = await logger.open(configs);
    expect(success).toBeTruthy();
  });
});

describe('VirtuinLogger logging levels', () => {
  const logger = new VirtuinLogger();
  beforeAll(async () => {
    process.env.VIRT_STATION_NAME = process.env.VIRT_STATION_NAME || 'VirtionDebug';
    logger.vtEnableCloudwatch = false;
    const configs = {
      test: {
        testUUID: '123456789',
      },
      station: {
        name: 'debug_station',
      },
      dut: {
      },
    };
    await logger.open(configs);
    return logger;
  });

  afterAll(() => {
    logger.close();
  });

  it('should successfully be connected successfully', async () => {
    expect.assertions(1);
    const status = await logger.status();
    expect(status).toMatchObject({
      open: true,
      error: null,
    });
  });

  it('should successfully log info msg', async () => {
    expect.assertions(1);
    const success = await logger.log('Info log message!', 'info');
    expect(success).toBeTruthy();
  });

  it('should successfully log warn msg', async () => {
    expect.assertions(1);
    const success = await logger.log('Warning log message', 'warn');
    expect(success).toBeTruthy();
  });

  it('should successfully log error msg', async () => {
    expect.assertions(1);
    const success = await logger.log('Error log message', 'error');
    expect(success).toBeTruthy();
  });

  it('should fail log unknown log level', async () => {
    expect.assertions(2);
    const success = await logger.log('Invalid log message level', 'invalid-level');
    expect(success).toBeFalsy();
    const status = await logger.status();
    expect(status).toMatchObject({
      open: true,
      error: expect.any(String),
    });
  });
});
