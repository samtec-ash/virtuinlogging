/**
 * @file Virtuin Logger consists of one classes: VirtuinLogger.
 * VirtuinLogger shall be used to send remote logging to AWS CloudWatch
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 * @flow
 */

const winston = require('winston');
const WinstonCloudWatch = require('winston-cloudwatch');

interface TransportType {
  kthxbye(cb: (void) => void): void;
}

/**
 * @classdesc Used to send remote logging to AWS CloudWatch.
 * @author Adam Page <adam.page@samtec.com>
 * @version 0.8.0
 */
class VirtuinLogger {
  // eslint-disable-next-line
  static LEVELS = ['silly', 'debug', 'info', 'warn', 'error'];

  meta: ?Object;

  logger: ?Object;

  consoleTransport: null | Object;

  awsCloudTransport: null | TransportType;

  awsRetryTimer: ?TimeoutID;

  errorMsg: ?string;

  vtStationGroup: boolean;

  vtEnableCloudwatch: boolean;

  retryDelay: number;

  vtDebug: boolean;

  groupName: string;

  streamName: string;

  constructor() {
    this.meta = null;
    this.logger = null;
    this.consoleTransport = null;
    this.awsCloudTransport = null;
    this.awsRetryTimer = null;
    this.errorMsg = null;
    this.vtStationGroup = false;
    this.vtEnableCloudwatch = true;
    this.retryDelay = 5000;
    this.vtDebug = false;
    this.groupName = '';
    this.streamName = '';
  }

  /**
   * Open remote logging connection
   * @async
   * @param {object} config - Contains {station: {name}, dut: {}, test: { testUUID }}
   * @return {bool} If successfully opened
   */
  open = async (config: Object) => {
    try {
      const test = config ? config.test : {};
      const station = config ? config.station : {};
      const stationName = station.name || process.env.VIRT_STATION_NAME || undefined;
      if (stationName === undefined) {
        throw new Error('Station name not provided nor defined in env');
      }
      if (test.testUUID) {
        this.setRootMeta({
          testUUID: test.testUUID
        });
      }

      this.groupName = `${this.vtStationGroup ? 'VirtuinStation' : 'VirtuinTest'}${this.vtDebug ? 'Debug' : ''}`;
      this.streamName = `${stationName}`;

      this.logger = winston.createLogger({
        level: 'info',
        format: winston.format.json(),
        transports: []
      });

      this.createConsoleTransport();
      if (this.vtEnableCloudwatch) {
        this.createAWSCloudTransport();
      }
      this.errorMsg = null;
      return true;
    } catch (err) {
      this.logger = null;
      this.errorMsg = `Error opening: ${err.message}`;
      return false;
    }
  }

  createConsoleTransport = () => {
    if (this.consoleTransport && this.logger && this.logger.transports.length) {
      this.logger.remove(this.consoleTransport);
      this.consoleTransport = null;
    }
    if (this.logger) {
      this.consoleTransport = new winston.transports.Console({
        json: true,
        colorize: true,
        level: 'debug'
      });
      this.logger.add(this.consoleTransport);
    }
  }

  createAWSCloudTransport = () => {
    if (this.awsCloudTransport && this.logger && this.logger.transports.length) {
      this.logger.remove(this.awsCloudTransport);
      this.awsCloudTransport = null;
    }
    if (this.logger) {
      const awsRegion = process.env.AWS_REGION || 'us-east-2';
      const awsAccessKeyId = process.env.AWS_ACCESS_KEY_ID || null;
      const awsSecretKey = process.env.AWS_SECRET_ACCESS_KEY || process.env.AWS_SECRET_KEY || null;
      const logGroupName = this.groupName;
      const logStreamName = this.streamName;

      this.awsCloudTransport = new WinstonCloudWatch({
        logGroupName,
        logStreamName,
        awsAccessKeyId,
        awsSecretKey,
        awsRegion,
        errorHandler: this.awsCloudErrorHandler,
        jsonMessage: true,
        level: 'info'
      });
      this.logger.add(this.awsCloudTransport);
    }
  }

  awsCloudErrorHandler = (error: Error) => {
    // Close transport and retry later
    console.error(`AWS CloudWatch Error: ${error.message}`);
    if (this.awsCloudTransport && this.logger && this.logger.transports.length) {
      this.logger.remove(this.awsCloudTransport);
    }
    if (this.awsCloudTransport) {
      this.awsCloudTransport.kthxbye(() => {
        this.awsRetryTimer = setTimeout(this.createAWSCloudTransport, this.retryDelay);
        this.awsCloudTransport = null;
      });
    }
  }

  /**
   * Set root meta to be added to every log message
   * @param {Object | null} meta Serializable object or null
   */
  setRootMeta = (meta: Object | null) => {
    if (typeof meta === 'object' || meta === null) {
      this.meta = meta;
    }
  }

  /**
   * Clear errors
   * @return {None}
   */
  clear = () => {
    this.errorMsg = null;
  }

  /**
   * Get logging status
   * @return {Object} {open: bool, error: ?string}
   */
  status = () => (
    {
      open: this.logger != null,
      error: this.errorMsg
    }
  )

  /**
   * Close remote logging connection
   * @async
   * @return {bool} If successfully closed
   */
   close = async () => {
     if (this.awsRetryTimer) {
       clearTimeout(this.awsRetryTimer);
     }
     await new Promise(resolve => {
       if (this.awsCloudTransport) {
         this.awsCloudTransport.kthxbye(() => {
           resolve();
         });
       } else {
         resolve();
       }
     });

     if (this.logger) {
       const { logger } = this;
       logger.clear();
       logger.close();
     }
     this.logger = null;
     this.consoleTransport = null;
     this.awsCloudTransport = null;
     this.errorMsg = null;
     return true;
   }

  silly = async (msg: string, meta: ?Object = null) => this.log(msg, 'silly', meta)

  debug = async (msg: string, meta: ?Object = null) => this.log(msg, 'debug', meta)

  info = async (msg: string, meta: ?Object = null) => this.log(msg, 'info', meta)

  warn = async (msg: string, meta: ?Object = null) => this.log(msg, 'warn', meta)

  error = async (msg: string, meta: ?Object = null) => this.log(msg, 'error', meta)


  /**
   * Sends log message
   * @async
   * @param {string} msg - Log message
   * @param {string} level - Log level (silly, debug, info, warn, error)
   * @param {Object} meta - Log metadata object
   * @return {bool} Success
   */
  log = async (msg: string, level: string = 'info', meta: ?Object = null) => {
    if (this.logger == null) {
      this.errorMsg = 'Must successfully call open before logging';
      return false;
    }
    const { logger } = this;
    if (VirtuinLogger.LEVELS.find(l => l === level.toLowerCase()) === undefined) {
      this.errorMsg = 'Log level is invalid';
      return false;
    }
    const logLevel = level;
    const logMsg = msg;
    let logMeta = this.meta ? this.meta : {};
    logMeta = { ...meta, ...logMeta };

    logger.log({
      level: logLevel,
      message: logMsg,
      meta: logMeta
    });
    return true;
  }
}

export { VirtuinLogger };
export default VirtuinLogger;
